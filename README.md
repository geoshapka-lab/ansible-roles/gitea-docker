Gitea docker
=========

This role will install Gitea in docker compose with ssl cert

Requirements
------------

server with docker and traefik

Role Variables
--------------

See in defaults

Example Playbook
----------------

```yaml
- name: Install gitea docker
  hosts: gitea
  tags: gitea

  vars:
    traefik_zone: internal.zone
    gitea_email: "invalid-test@gmail.com"

  roles:
    - role: geoshapka.gitea-docker
```

Author Information
------------------

Made by Georgy Shapiro
contact me at geoshapka@gmail.com
